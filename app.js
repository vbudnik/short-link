const express = require('express');
const config = require('config');
const path = require('path')
const mongoose = require('mongoose')

// Routes
const Auth = require('./routes/auth.routes')
const Link = require('./routes/link.routes')
const Redirect = require('./routes/redirect.routes')

const app = express();

app.use(express.json({ extended: true}));

app.use('/api/auth',  Auth);
app.use('/api/link',  Link);
app.use('/t',  Redirect);

if (process.env.NODE_ENV  === "production") {
   app.use('/', express.static(path.join(__dirname, 'client', 'build')));
   app.get('*', (req, res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
   })
}

const PORT = config.get('port') || 5000;

async function start() {
   try {
      await mongoose.connect(config.get('mongoUri'), {
         useNewUrlParser: true,
         useUnifiedTopology: true,
         useCreateIndex: true
      });

      // start server
      app.listen(PORT, () => {
         console.log(`Start server on port ${PORT}`);
      });
   } catch (e) {
      console.log('Server error', e.message);
      process.exit(1);
   }
};
start();
