import React from "react";

export const LinkCart = ({ link}) => {
    return (
        <>
            <h2>Link</h2>
            <p>Link To: <a herf={link.to} target="_blank" rel="noopener noreferrer">{link.to}</a></p>
            <p>Link From: <a herf={link.from} target="_blank" rel="noopener noreferrer">{link.from}</a></p>
            <p>Count Click: <strong>{link.clicks}</strong></p>
            <p>Date create: <strong>{new Date(link.date).toLocaleDateString()}</strong></p>
        </>
    )
};